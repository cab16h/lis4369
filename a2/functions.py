#!/usr/bin/env_python3

print("Payroll Calculator")

print("\nProgram Requirements:")
print("1. Must use float data type for user input.")
print("2. Overtime rate: 1.5 times hourly rate (hours over 40).")
print("3. Holiday rate: 2.0 times hourly rate (all holiday hours).")
print("4. Must format currency with dollar sign, and round to two decimal places.")
print("5. Create at least three functions that are called by the program: ")
print("\ta. main(): calls at least two other functions.")
print("\tb. get_requirements(): displays the program requirements.")
print("\tc. calculate_payrool(): calculates an individual one-week paycheck.")

print("\nInput:")
hours = float( input("Enter hours worked: " ))
hol_hours = float( input("Enter holiday hours: " ))
pay_rate = float( input("Enter hourly pay rate: " ))

if hours <= 40:
    overtime = 0.00
    base = hours * pay_rate
    holiday = (pay_rate * hol_hours) * 2.0
    gross = base + holiday
    print("\nOutput: ")
    print("{0:17} ${1:>5.2f}".format("Base:", base))
    print("{0:17} ${1:>5.2f}".format("Ovetime:", overtime))
    print("{0:17} ${1:>5.2f}".format("Holiday:", holiday))
    print("{0:17} ${1:>5.2f}".format("Gross:", gross))
elif hours > 40:
    base = 40 * pay_rate
    overtime_rate = 1.5
    over = hours - 40
    overtime = (over * overtime_rate) * pay_rate
    holiday = (pay_rate * hol_hours) * 2.0
    gross = base + overtime + holiday
    print("\nOutput: ")
    print("{0:17} ${1:>5.2f}".format("Base:", base))
    print("{0:17} ${1:>5.2f}".format("Ovetime:", overtime))
    print("{0:17} ${1:>5.2f}".format("Holiday:", holiday))
    print("{0:17} ${1:>5.2f}".format("Gross:", gross))
    