

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Christian Burell - Information Technology Major

### Assignment 2 Requirements:


#### README.md file should include the following items:

1. Run payroll calculator 
2. Backward-enginneer the picture on a2.
3. Provide screenshots of payroll calculator.






#### Assignment Screenshots:


*Screenshot of Payroll Calculator*:

![Normal Hours screenshot](img/reg.png)

*Screenshot of Payroll Calculator*:

![Overtime Hours](img/over.png)

*Screenshot of Payroll Calculator IDLE*:

![Normal Hours](img/normidle.png)

*Screenshot of Payroll Calculator IDLE*:

![Overtime Hours](img/overidle.png)

