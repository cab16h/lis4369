

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Christian Burell - Information Technology Major

### Assignment 5 Requirements:


#### README.md file should include the following items:

1. Screenshot of titanic graphs
2. Screenshot of tutorial pictures




#### Assignment Screenshot and Links:
Graph 1                          |  Graph 2
:------------------------------------------:|:------------------------------------------:
![1](img/titanic1.png)     |![2](img/titanic2.png)

Tutorial                        |  Tutorial
:------------------------------------------:|:------------------------------------------:
![1](img/dot.png)     |![2](img/dot2.png)

Tutorial                        |  Tutorial
:------------------------------------------:|:------------------------------------------:
![1](img/command.png)     |![2](img/command2.png)

Tutorial                        |  Tutorial
:------------------------------------------:|:------------------------------------------:
![1](img/command3.png)     |![2](img/full.png)

Tutorial                        |  Tutorial
:------------------------------------------:|:------------------------------------------:
![1](img/graph1.png)     |![2](img/graph2.png)

