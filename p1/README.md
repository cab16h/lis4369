

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Project 1 Requirements:


#### README.md file should include the following items:

1. Distributed Version Control with Git and Bitbucket
2. Develop an application that runs a data analysis
3. Include a graph to display data


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

 

#### Assignment Screenshots:


*Screenshot of terminal*:

![Terminal](img/first.png)

*Screenshot of terminal*:

![Terminal](img/second.png)

*Screenshot of graph*:

![Terminal](img/graph.png)



