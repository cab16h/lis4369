#!/usr/bin/env_python3

print("Calorie Percentage")

print("\nProgram Requirements:")
print("1. Find calories per grams of fat, carbs, and protein.")
print("2. Calculate percentages.")
print("3. Must use float data types.")
print("4. Format, right-alighn numbers, and round to two decimal places.")

print("\nInput:")
fat = float( input("Enter total fat grams: " ))
carb = float( input("Enter total carb grams: " ))
protein = float( input("Enter total protein grams: " ))
fat_cal = fat * 9
carb_cal = carb * 4
pro_cal = protein * 4
total = int(fat + carb + protein)
##fat_per = (fat / total)
##carb_per = (carb / total)
##pro_per = (protein / total)
total_cal = (fat_cal + carb_cal + pro_cal)
fat_perc = (fat_cal / total_cal) * 100
carb_perc = (carb_cal / total_cal) * 100
pro_perc = (pro_cal / total_cal) * 100

print("\nOutput:")
print("{0:20} {1:20} {2:20}".format("Type", "\tCalories", "\tPercentage"))
print("{0:23} {1:>,.2f} {2:>24.2f}%".format("Fat", fat_cal, fat_perc))
print("{0:23} {1:>,.2f} {2:>24.2f}%".format("Carb", carb_cal, carb_perc))
print("{0:23} {1:>,.2f} {2:>24.2f}%".format("Protein", pro_cal, pro_perc))
