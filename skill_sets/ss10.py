
def my_requirements():
	print("Python Dictionaries")
	print()
	print("Program Requirements:")
	print("1. Dictionaries (Python data structure): unordered key:value pairs.")
	print("2. Dictionary: an associative array (also known as hashes).")
	print("3. Any key in a dictionary is associated (or mapped) to a value (i.e., any Python data type).")
	print("4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unqiue.")
	print("5. Values: can be any data type and can repeat.")
	print("6. Create a program that mirrors the following IPO (input/process/output) format.")
	print("\tCreate empty dictionary, using curly braces {}: my_dictionary = {}")
	print("\tUse the following keys: fname, lname, degree, major, gpa")
	print("Note: Dictionaries have key-value pairs instead of single values; this differentiatives a dictionary from a set.")
	print()

def my_output():
	my_dictionary = {}

	fname1 = input("First name: ")
	key = "fname"
	my_dictionary[key] = fname1

	lname1 = input("Last Name: ")
	key1 ="lname"
	my_dictionary[key1]= lname1

	degree1 = input("Degree: ")
	key2 = "degree"
	my_dictionary[key2] = degree1

	major1 = input("Major (IT or ICT): ")
	key3 = "major"
	my_dictionary[key3] = major1

	gpa1 = input("GPA: ")
	key4 = "gpa"
	my_dictionary[key4] = gpa1

	print()
	print("Output:")
	print("Print my_dictionary:")
	print(my_dictionary)

	print()
	print("Return view of dictionary's (key, value) pair, built-in function:")
	print(my_dictionary.items())

	print()
	print("Return view object of all keys, built-in function: ")
	print(my_dictionary.keys())

	print()
	print("Return view object of all values in dictionary, built-in function:")
	print(my_dictionary.values())

	print()
	print("Print only first and last names, using keys:")
	print(my_dictionary[key] + " " + my_dictionary[key1])


	print()
	print("Print only first and last names, using get() function:")
	print(my_dictionary.get(key) + " " + my_dictionary.get(key1))

	print()
	print("Count number of items (key:value pairs) in dictionary:")
	print(len(my_dictionary))

	print()
	print("Remove last dictionary item (popitem):")
	my_dictionary.pop(key4)
	print(my_dictionary)

	print()
	print("Delete major from dictionary, using key:")
	del my_dictionary[key3]
	print(my_dictionary)

	print()
	print("Return object type:")
	print(type(my_dictionary))

	print()
	print("Delete all items from list:")
	my_dictionary.clear()
	print(my_dictionary)

def main():
	my_requirements()
	my_output()

if __name__ == '__main__':
	main()