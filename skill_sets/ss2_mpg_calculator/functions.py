#!/usr/bin/env_python3

print("Miles Per Gallon")
print("\nProgram Requirements:\n"
    + "1. Convert MPG.\n"
    + "2. Must use float data type for user input and calculation.\n"
    + "3. Format and round conversion  two decimal places.\n")

mi_driven = float( input("Input: \nEnter miles driven: " ))
used = float( input("Enter gallons of fuel used: " ))

mpg = (mi_driven / used) * 1

print("\nOutput: ")
print('{:,.2f}'.format(mi_driven), 'miles driven and', '{:,.2f}'.format(used), 'gallons used = ', '{:,.2f}'.format(mpg), 'mpg')


