

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Christian Burell - Information Technology Major

### Assignment 3 Requirements:


#### README.md file should include the following items:

1. Run Painting Estimator
2. Screenshots on terminal and idle
3. Create at least five functions that are called by the program  
    - main(): calls two other functions: get(requirements() and estimate_painting_cost().
    - get_requirements(): displays the program requirements
    - estimate_painting_cost(): calculates interior home painting, and calls print function
    - print_painting_estimate(): displays painting costs
    - print_painting_percentage(): displays painting costs percentages
 





#### Assignment Screenshot and Links:
*Screenshot A3*:
![Terminal](img/terminal.png "Painting Estimator in terminal")


*Screenshot A3*:
![Idle](img/idle.png "Painting Estimator in idle")






