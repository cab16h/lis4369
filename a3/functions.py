

import functions as f

def main():
    f.get_requirements()
    f.estimate_painting_cost()

if __name__ == "__main__":
    main()




def get_requirements():
    
    print("Payroll Calculator\n")

    print("Program Requirements:")
    print("1.   Calculate home interior paint cost (w/o primer.")
    print("2.   Must use float data types.")
    print("3.   Must use SQFT_PER_GALLON constant (350).")
    print("4.   Must use iteration structure (aka \"loop\").")
    print("5.   Format, right-align numbers, and round to two decimal places.")
    print("6.   Create at least five functions that are called by the program:")
    print("\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost().")
    print("\tb. get_requirements(): displays the program requirements.")
    print("\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.")
    print("\td. print_painting_estimate(): displays painting costs.")
    print("\te. print_painting_percentage(): displays painting costs percentages.")

def estimate_painting_cost():

    repeat = "Y"
    SQFT_PER_GALLON = 350

    while repeat == "Y":
        print("\nInput:")
        
        int_sqft = float(input("Enter total interior sq ft: "))
        price_per = float(input("Enter price per gallon paint: "))
        hour_rate = float(input("Enter hourly painting rate per sq feet: "))
               
                
        num_gal = int_sqft / SQFT_PER_GALLON
        paint_tot = num_gal * price_per
        labor_tot = int_sqft * hour_rate
        tot_tot = paint_tot + labor_tot
        paint_per = paint_tot / tot_tot * 100
        labor_per = labor_tot / tot_tot * 100
        tot_per = tot_tot / tot_tot * 100

        
        print_painting_estimate(int_sqft, SQFT_PER_GALLON, num_gal, price_per, hour_rate)
        print_painting_percentage(paint_tot, paint_per, labor_tot, labor_per, tot_tot, tot_per)

        repeat = input("Estimate another paint job? (y/n): ").upper()

        if repeat == "N":
            print("\nThank you for using our painting estimator!")
            print("Please see our website: ")
    
    
def print_painting_estimate(int_sqft, SQFT_PER_GALLON, num_gal, price_per, hour_rate):

    
    print("\nOutput")
    print("{0:8} {1:>20}".format("Item", "Amount"))
    print("{0:8} {1:15,.2f}".format("Total Sq Ft: ", int_sqft))
    print("{0:8} {1:10,.2f}".format("Sq ft per Gallon: ", SQFT_PER_GALLON))
    print("{0:8} {1:9,.2f}".format("Number of Gallons: ", num_gal))
    print("{0:8} {1:8,.2f}".format("Paint per Gallon:  $", price_per))
    print("{0:8} {1:8,.2f}".format("Labor per Sq Ft:   $", hour_rate))

def print_painting_percentage(paint_tot, paint_per, labor_tot, labor_per, tot_tot, tot_per):

   
    print("{0:8} {1:>13} {2:>16}".format("\nCost", "Amount", "Percentage"))
    print("{0:12} {1:8,.2f} {2:15.2f}%".format("Paint:     $", paint_tot, paint_per))
    print("{0:10} {1:8,.2f} {2:15.2f}%".format("Labor:     $", labor_tot, labor_per))
    print("{0:12} {1:6,.2f} {2:15.2f}%".format("Total:     $", tot_tot, tot_per))