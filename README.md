> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369 EXTENSIBLE ENTERPRISE SOLUTIONS

## Christian Burell

### Lis 4369 Requirements: 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - create a1_tip_calculator application
    - Provide screenshots of installation 
    - Create bitbucket repo
    - Complete bitbucket tutorial 
    - Provide git commands

2. [A2 README.md](a2/README.md "My A1 README.md file")
    - Screenshots of payroll calculator
    - Run payroll calculator on terminal and idle


3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of Painting Estimator
    - Run Painting Estimator on terminal and idle
    

4. [P1 README.md](p1/README.md "My p1 README.md file")
    - Install Python packages like pip and pandas
    - Create a program that runs an analysis on a set of data
    - Include a graph
    

5. [A4 README.md](a4/README.md "My a4 README.md file")
    - Code and run demo.py.
    - Then. use demo.py to backward-engineer the screenshots below it.


6. [A5 README.md](a5/README.md "My a5 README.md file")
    - Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
    - Code and run lis4369_a5.R (see below)
    - Be sure to include at least two plots in your README.md file.

   
7. [P2 README.md](p2/README.md "My p2 README.md file")
    - tbd
