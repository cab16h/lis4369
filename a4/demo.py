from data_analysis_2 import data_analysis_2
from requirements import get_requirements

def main():
	get_requirements()
	data_analysis_2()


if __name__ == '__main__':
	main()
	input("Press any key to exit")