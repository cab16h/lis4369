

> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Christian Burell - Information Technology Major

### Assignment 4 Requirements:


#### README.md file should include the following items:

1. Display graph
2. Screenshots of demo.py in IDLE and Visual Studio Code
3. Install Packages 

    
 #### Assignment Screenshot and Links:
*Screenshot A4*:
![Graph](img/graph.png "Graph")

*Screenshot A4*:
![Terminal](img/first.png "Screenshot of terminal")

IDLE                        |  IDLE
:------------------------------------------:|:------------------------------------------:
![graph](img/idle1.png)     |![Screenshot](img/idle2.png)




